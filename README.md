[![pipeline status](https://git.coop/aptivate/ansible-roles/nginx/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/nginx/commits/master)

# nginx

A role to install the NGINX web server.

# Requirements

None.

# Dependencies

* https://git.coop/aptivate/ansible-roles/epel-repository

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: nginx
```

# Testing

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
